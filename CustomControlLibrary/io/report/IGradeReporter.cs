﻿namespace CustomControlLibrary.io.report
{
    /// <summary>
    /// Classes implementing this interface generate reports based on the data
    /// in a GradeControl.
    /// </summary>
    /// <remarks>
    /// Author:     David Jarrett
    /// Version:    01/26/2018
    /// </remarks>
    public interface IGradeReporter
    {
        /// <summary>
        /// Generates the report.
        /// </summary>
        /// <returns></returns>
        string GenerateReport();

        /// <summary>
        /// Gets the maximum possible points.
        /// </summary>
        /// <returns></returns>
        int GetMaxPossiblePoints();

        /// <summary>
        /// Gets the total points earned.
        /// </summary>
        /// <returns></returns>
        int GetTotalPointsEarned();
    }
}
