﻿using System;
using System.Text;

namespace CustomControlLibrary.io.report
{
    /// <summary>
    /// This class will generate a basic report for a single GradeControl.
    /// The report has the form - CONTROL TITLE: possiblePoints/MaximumPoints
    /// </summary>
    /// <remarks>
    /// Author:     David Jarrett
    /// Version:    01/26/2018
    /// </remarks>
    /// <seealso cref="IGradeReporter" />
    public class GradeReporter : IGradeReporter
    {
        private readonly GradeControl gradeControl;

        private readonly int totalPointsEarned;
        private readonly int maxPossiblePoints;

        /// <summary>
        /// Initializes a new instance of the <see cref="GradeReporter"/> class.
        /// </summary>
        /// <remarks>
        /// Precondition:   gradeControl != null
        /// Postcondition:  GradeReporter will be initialized and ready to report.
        /// </remarks>
        /// <param name="gradeControl">The grade control to be reported on.</param>
        /// <exception cref="ArgumentNullException">gradeControl</exception>
        public GradeReporter(GradeControl gradeControl)
        {
            this.gradeControl = gradeControl ?? throw new ArgumentNullException(nameof(gradeControl));
            this.maxPossiblePoints = gradeControl.MaximumPointsEarnable;
            this.totalPointsEarned = gradeControl.PointValueOfSelectedRadioButton;
        }

        /// <summary>
        /// Generates the report.
        /// </summary>
        /// <remarks>
        /// Precondition:   None
        /// </remarks>
        /// <returns>The report.</returns>
        public string GenerateReport()
        {
            var report = new StringBuilder(this.gradeControl.Title).Append(": ");

            report.Append(this.getPointBreakdown());
            report.Append(this.getComments());

            return report.ToString();
        }

        private StringBuilder getPointBreakdown()
        {
            var pointBreakdown = new StringBuilder();

            pointBreakdown.Append(this.totalPointsEarned);
            pointBreakdown.Append("/");
            pointBreakdown.Append(this.maxPossiblePoints);
            pointBreakdown.Append(Environment.NewLine);

            return pointBreakdown;
        }

        private StringBuilder getComments()
        {
            var comments = new StringBuilder();

            foreach (var comment in this.gradeControl.GetAllComments(true))
            {
                comments.Append(comment);
                comments.Append(Environment.NewLine);
            }
            return comments;
        }

        /// <summary>
        /// Gets the maximum possible points.
        /// </summary>
        /// <returns></returns>
        public int GetMaxPossiblePoints()
        {
            return this.maxPossiblePoints;
        }

        /// <summary>
        /// Gets the total points earned.
        /// </summary>
        /// <returns></returns>
        public int GetTotalPointsEarned()
        {
            return this.totalPointsEarned;
        }

    }
}
