﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace CustomControlLibrary.io.serialize
{
    /// <summary>
    /// This class provides static methods for serializing and deserializing the comments
    /// from any number of GradeControl objects.
    /// </summary>
    /// <remarks>
    /// Author:     David Jarrett
    /// Version:    01/26/2018
    /// </remarks>
    public class CommentSerializer
    {
        private static readonly string SerializationFilePath = Environment.CurrentDirectory 
            + "//SerializedComments.xml";

        /// <summary>
        /// Serializes the comments of the specified grade controls.
        /// </summary>
        /// <remarks>
        /// Precondition:   gradeControls != null
        /// Postcondition:  The comments in each control will be serialized to the SerializationFile.
        /// </remarks>
        /// <param name="gradeControls">The grade controls.</param>
        public static void Serialize(List<GradeControl> gradeControls)
        {
            if (gradeControls == null)
            {
                return;
            }
            var commentCollections = new List<CommentCollection>();

            foreach (var gradeControl in gradeControls)
            {
                var commentsForCurrentControl = gradeControl.GetAllComments(false);
                var commentCollection = new CommentCollection { Comments = commentsForCurrentControl };
                commentCollections.Add(commentCollection);
            }

            writeOut(commentCollections);
        }

        private static void writeOut(IReadOnlyCollection<CommentCollection> commentCollections)
        {
            var writer = new XmlSerializer(typeof(List<CommentCollection>));
            var file = File.Create(SerializationFilePath);

            writer.Serialize(file, commentCollections);
            file.Close();
        }

        /// <summary>
        /// Deserializes all comments from the SerializationFile.
        /// </summary>
        /// <returns>A list of CommentCollection objects containing the deserialized comments,
        ///          or null if the SerializationFile is not found.</returns>
        public static List<CommentCollection> Deserialize()
        {
            try
            {
                var reader = new XmlSerializer(typeof(List<CommentCollection>));
                var file = new StreamReader(SerializationFilePath);

                var comments = reader.Deserialize(file) as List<CommentCollection>;
                file.Close();
                return comments;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        /// <summary>
        /// Represents a collection of comments.
        /// </summary>
        /// <remarks>
        /// Author:     David Jarrett
        /// Version:    01/26/2018
        /// </remarks>
        public class CommentCollection
        {
            /// <summary>
            /// Gets or sets the comments.
            /// </summary>
            /// <value>
            /// The comments.
            /// </value>
            public List<string> Comments { get; set; }
        }

    }
}
