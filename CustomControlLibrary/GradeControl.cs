﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CustomControlLibrary.io.report;

namespace CustomControlLibrary
{
    /// <summary>
    /// This control is used to help grade student assignments. It consists of radio buttons that
    /// represent various customizable scores, as as well a DataGridView that contains comments which
    /// can be included in a report.
    /// </summary>
    /// <remarks>
    /// Author:  David Jarrett
    /// Version: 01/26/2018
    /// </remarks>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    /// <seealso cref="GradeReporter" />
    public partial class GradeControl: UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GradeControl"/> class.
        /// </summary>
        public GradeControl()
        {
            this.InitializeComponent();
            this.initializeRadioButtons();
            this.Title = "Default";
        }

        private void initializeRadioButtons()
        {
            foreach (var radioButton in this.getAllRadioButtons())
            {
                radioButton.Tag = new RadioButtonPointValue();
            }

            this.ExceptionalRadioButtonPointValue = 3;
            this.AcceptableRadioButtonPointValue = 2;
            this.AmateurRadioButtonPointValue = 1;
            this.UnsatisfactoryRadioButtonPointValue = 0;
        }

        /// <summary>
        /// Occurs when any data in the control is modified.
        /// </summary>
        public event EventHandler DataStateChanged;
        
        /// <summary>
        /// Gets or sets the exceptionalRadioButton point value.
        /// </summary>
        /// <value>
        /// The exceptionalRadioButton point value.
        /// </value>
        public int ExceptionalRadioButtonPointValue
        {
            get => ((RadioButtonPointValue) this.exceptionalRadioButton.Tag).PointValue;
            set
            {
                ((RadioButtonPointValue)this.exceptionalRadioButton.Tag).PointValue = value;
                this.exceptionalRadioButton.Text = this.generateName("Exceptional", value);
            }
        }
        
        /// <summary>
        /// Gets or sets the acceptableRadioButton point value.
        /// </summary>
        /// <value>
        /// The acceptableRadioButton point value.
        /// </value>
        public int AcceptableRadioButtonPointValue
        {
            get => ((RadioButtonPointValue)this.acceptableRadioButton.Tag).PointValue;
            set
            {
                ((RadioButtonPointValue) this.acceptableRadioButton.Tag).PointValue = value;
                this.acceptableRadioButton.Text = this.generateName("Acceptable", value);
            }
        }

        /// <summary>
        /// Gets or sets the amateurRadioButton point value.
        /// </summary>
        /// <value>
        /// The amateurRadioButton point value.
        /// </value>
        public int AmateurRadioButtonPointValue
        {
            get => ((RadioButtonPointValue)this.amateurRadioButton.Tag).PointValue;
            set
            {
                ((RadioButtonPointValue)this.amateurRadioButton.Tag).PointValue = value;
                this.amateurRadioButton.Text = this.generateName("Amateur", value);
            }
        }

        /// <summary>
        /// Gets or sets the unsatisfactoryRadioButton point value.
        /// </summary>
        /// <value>
        /// The unsatisfactoryRadioButton point value.
        /// </value>
        public int UnsatisfactoryRadioButtonPointValue
        {
            get => ((RadioButtonPointValue)this.unsatisfactoryRadioButton.Tag).PointValue;
            set
            {
                ((RadioButtonPointValue) this.unsatisfactoryRadioButton.Tag).PointValue = value;
                this.unsatisfactoryRadioButton.Text = this.generateName("Unsatisfactory", value);
            }
        }

        private string generateName(string title, int value)
        {
            return "(" + value + ") " + title;
        }

        /// <summary>
        /// Gets the point value of the selected radio button.
        /// </summary>
        /// <value>
        /// The selected RadioButton point value.
        /// </value>
        /// <exception cref="System.NullReferenceException">radioButtonPointValue</exception>
        public int PointValueOfSelectedRadioButton
        {
            get
            {
                var radioButton = this.getSelectedRadioButton();
                var radioButtonPointValue = (RadioButtonPointValue) radioButton.Tag 
                    ?? throw new NullReferenceException(nameof(radioButton.Tag));

                return radioButtonPointValue.PointValue;
            }
        }

        private RadioButton getSelectedRadioButton()
        {
            return this.pointValueRadioButtonGroupBox.Controls.OfType<RadioButton>().
                        FirstOrDefault(r => r.Checked);
        }

        /// <summary>
        /// Gets the maximum points earnable from this control.
        /// </summary>
        /// <value>
        /// The maximum points earnable.
        /// </value>
        public int MaximumPointsEarnable
        {
            get
            {
                var radioButtons = this.getAllRadioButtons();
                var maxPointValue = 0;

                foreach (var radioButton in radioButtons)
                {
                    var currentRadioButtonPointValue = ((RadioButtonPointValue) radioButton.Tag).PointValue;
                    maxPointValue = Math.Max(currentRadioButtonPointValue, maxPointValue);
                }

                return maxPointValue;
            }
        }

        private IEnumerable<RadioButton> getAllRadioButtons()
        {
            return this.pointValueRadioButtonGroupBox.Controls.OfType<RadioButton>().AsEnumerable();
        }

        private class RadioButtonPointValue
        {
            public int PointValue { get; set; }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Resets the control.
        /// </summary>
        /// <remarks>
        /// Precondition:   None
        /// Postcondition:  this.exceptionalRadioButton.checked == true AND all comments in the control
        ///                 will be unchecked.
        /// </remarks>
        public void ResetControl()
        {
            this.exceptionalRadioButton.Checked = true;
            this.setAllCommentsCheckmarkState(false);
        }

        /// <summary>
        /// Gets all comments in this control.
        /// </summary>
        /// <remarks>
        /// Precondition:   None
        /// </remarks>
        /// <param name="onlyReturnCheckedComments">if set to <c>true</c> [only return checked comments].</param>
        /// <returns>All comments in this control.</returns>
        public List<string> GetAllComments(bool onlyReturnCheckedComments)
        {
            var comments = new List<string>();
            foreach (DataGridViewRow row in this.commentGridView.Rows)
            {
                this.addCommentFromRow(row, comments, onlyReturnCheckedComments);
            }
            return comments;
        }

        private void addCommentFromRow(DataGridViewRow row, ICollection<string> comments, bool onlyReturnCheckedComments)
        {
            var checkmarkCell = (DataGridViewCheckBoxCell) row.Cells[0];

            if (!onlyReturnCheckedComments)
            {
                this.addCommentToCollection(row, comments);
            }
            else if (Convert.ToBoolean(checkmarkCell.EditedFormattedValue))
            {
                this.addCommentToCollection(row, comments);
            }
        }

        private void addCommentToCollection(DataGridViewRow row, ICollection<string> comments)
        {
            var textCell = (DataGridViewTextBoxCell) row.Cells[1];
            if (textCell.Value != null)
            {
                comments.Add(textCell.Value.ToString());
            }
        }

        /// <summary>
        /// Adds a new comment to the control.
        /// </summary>
        /// <remarks>
        /// Precondition:  comment != null
        /// Postcondition: The control will contain a new comment.
        /// </remarks>
        /// <param name="comment">The comment.</param>
        /// <exception cref="ArgumentNullException">comment</exception>
        public void AddComment(string comment)
        {
            if (comment == null)
            {
                throw new ArgumentNullException(nameof(comment));
            }
            this.commentGridView.Rows.Add(false, comment);
        }

        /// <summary>
        /// Clears all comments.
        /// </summary>
        /// <remarks>
        /// Precondition:   None
        /// Postcondition:  All comments will be removed from the control.
        /// </remarks>
        public void ClearAllComments()
        {
            this.commentGridView.Rows.Clear();
        }

        private void uncheckAllCommentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.setAllCommentsCheckmarkState(false);
        }

        private void checkAllCommentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.setAllCommentsCheckmarkState(true);
        }

        private void setAllCommentsCheckmarkState(bool state)
        {
            foreach (DataGridViewRow row in this.commentGridView.Rows)
            {
                ((DataGridViewCheckBoxCell)row.Cells[0]).Value = state;
            }
            this.onDataStateChanged();
        }

        private void onDataStateChanged()
        {
            this.DataStateChanged?.Invoke(this, EventArgs.Empty);
        }

        private void commentGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.onDataStateChanged();
        }

        private void commentGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.onDataStateChanged();
        }

        private void exceptionalRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.onDataStateChanged();
        }

        private void acceptableRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.onDataStateChanged();
        }

        private void amateurRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.onDataStateChanged();
        }

        private void unsatisfactoryRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.onDataStateChanged();
        }

        private void deleteRowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.deleteSelectedRow();
            this.onDataStateChanged();
        }

        private void deleteSelectedRow()
        {
            foreach (DataGridViewRow row in this.commentGridView.SelectedRows)
            {
                this.commentGridView.Rows.Remove(row);
            }
        }

        private void commentGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            this.selectRowIfRightMouseClicked(e);
        }

        private void selectRowIfRightMouseClicked(DataGridViewCellMouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            this.commentGridView.ClearSelection();

            var selectedRow = e.RowIndex;
            if (selectedRow != -1)
            {
                this.commentGridView.Rows[selectedRow].Selected = true;
            }
        }
    }
}
