﻿namespace CustomControlLibrary
{
    partial class GradeControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pointValueRadioButtonGroupBox = new System.Windows.Forms.GroupBox();
            this.unsatisfactoryRadioButton = new System.Windows.Forms.RadioButton();
            this.amateurRadioButton = new System.Windows.Forms.RadioButton();
            this.acceptableRadioButton = new System.Windows.Forms.RadioButton();
            this.exceptionalRadioButton = new System.Windows.Forms.RadioButton();
            this.commentGridView = new System.Windows.Forms.DataGridView();
            this.selectCommentToIncludeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.commentToIncludeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkUncheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.checkAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uncheckAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pointValueRadioButtonGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.commentGridView)).BeginInit();
            this.checkUncheckContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // pointValueRadioButtonGroupBox
            // 
            this.pointValueRadioButtonGroupBox.Controls.Add(this.unsatisfactoryRadioButton);
            this.pointValueRadioButtonGroupBox.Controls.Add(this.amateurRadioButton);
            this.pointValueRadioButtonGroupBox.Controls.Add(this.acceptableRadioButton);
            this.pointValueRadioButtonGroupBox.Controls.Add(this.exceptionalRadioButton);
            this.pointValueRadioButtonGroupBox.Location = new System.Drawing.Point(0, 14);
            this.pointValueRadioButtonGroupBox.Name = "pointValueRadioButtonGroupBox";
            this.pointValueRadioButtonGroupBox.Size = new System.Drawing.Size(125, 116);
            this.pointValueRadioButtonGroupBox.TabIndex = 0;
            this.pointValueRadioButtonGroupBox.TabStop = false;
            this.pointValueRadioButtonGroupBox.Text = "Grade";
            // 
            // unsatisfactoryRadioButton
            // 
            this.unsatisfactoryRadioButton.AutoSize = true;
            this.unsatisfactoryRadioButton.Location = new System.Drawing.Point(7, 89);
            this.unsatisfactoryRadioButton.Name = "unsatisfactoryRadioButton";
            this.unsatisfactoryRadioButton.Size = new System.Drawing.Size(92, 17);
            this.unsatisfactoryRadioButton.TabIndex = 3;
            this.unsatisfactoryRadioButton.TabStop = true;
            this.unsatisfactoryRadioButton.Tag = "";
            this.unsatisfactoryRadioButton.Text = "Unsatisfactory";
            this.unsatisfactoryRadioButton.UseVisualStyleBackColor = true;
            this.unsatisfactoryRadioButton.CheckedChanged += new System.EventHandler(this.unsatisfactoryRadioButton_CheckedChanged);
            // 
            // amateurRadioButton
            // 
            this.amateurRadioButton.AutoSize = true;
            this.amateurRadioButton.Location = new System.Drawing.Point(7, 66);
            this.amateurRadioButton.Name = "amateurRadioButton";
            this.amateurRadioButton.Size = new System.Drawing.Size(64, 17);
            this.amateurRadioButton.TabIndex = 2;
            this.amateurRadioButton.TabStop = true;
            this.amateurRadioButton.Tag = "";
            this.amateurRadioButton.Text = "Amateur";
            this.amateurRadioButton.UseVisualStyleBackColor = true;
            this.amateurRadioButton.CheckedChanged += new System.EventHandler(this.amateurRadioButton_CheckedChanged);
            // 
            // acceptableRadioButton
            // 
            this.acceptableRadioButton.AutoSize = true;
            this.acceptableRadioButton.Location = new System.Drawing.Point(7, 43);
            this.acceptableRadioButton.Name = "acceptableRadioButton";
            this.acceptableRadioButton.Size = new System.Drawing.Size(79, 17);
            this.acceptableRadioButton.TabIndex = 1;
            this.acceptableRadioButton.TabStop = true;
            this.acceptableRadioButton.Tag = "";
            this.acceptableRadioButton.Text = "Acceptable";
            this.acceptableRadioButton.UseVisualStyleBackColor = true;
            this.acceptableRadioButton.CheckedChanged += new System.EventHandler(this.acceptableRadioButton_CheckedChanged);
            // 
            // exceptionalRadioButton
            // 
            this.exceptionalRadioButton.AutoSize = true;
            this.exceptionalRadioButton.Checked = true;
            this.exceptionalRadioButton.Location = new System.Drawing.Point(7, 20);
            this.exceptionalRadioButton.Name = "exceptionalRadioButton";
            this.exceptionalRadioButton.Size = new System.Drawing.Size(80, 17);
            this.exceptionalRadioButton.TabIndex = 0;
            this.exceptionalRadioButton.TabStop = true;
            this.exceptionalRadioButton.Tag = "";
            this.exceptionalRadioButton.Text = "Exceptional";
            this.exceptionalRadioButton.UseVisualStyleBackColor = true;
            this.exceptionalRadioButton.CheckedChanged += new System.EventHandler(this.exceptionalRadioButton_CheckedChanged);
            // 
            // commentGridView
            // 
            this.commentGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.commentGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.commentGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.commentGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectCommentToIncludeColumn,
            this.commentToIncludeColumn});
            this.commentGridView.ContextMenuStrip = this.checkUncheckContextMenuStrip;
            this.commentGridView.Location = new System.Drawing.Point(134, 14);
            this.commentGridView.Name = "commentGridView";
            this.commentGridView.RowHeadersVisible = false;
            this.commentGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.commentGridView.Size = new System.Drawing.Size(368, 262);
            this.commentGridView.TabIndex = 1;
            this.commentGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.commentGridView_CellContentClick);
            this.commentGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.commentGridView_CellEndEdit);
            this.commentGridView.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.commentGridView_CellMouseDown);
            // 
            // selectCommentToIncludeColumn
            // 
            this.selectCommentToIncludeColumn.HeaderText = "Add";
            this.selectCommentToIncludeColumn.Name = "selectCommentToIncludeColumn";
            this.selectCommentToIncludeColumn.Width = 32;
            // 
            // commentToIncludeColumn
            // 
            this.commentToIncludeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.commentToIncludeColumn.HeaderText = "Message";
            this.commentToIncludeColumn.Name = "commentToIncludeColumn";
            // 
            // checkUncheckContextMenuStrip
            // 
            this.checkUncheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkAllToolStripMenuItem,
            this.uncheckAllToolStripMenuItem,
            this.deleteRowToolStripMenuItem});
            this.checkUncheckContextMenuStrip.Name = "checkUncheckContextMenuStrip";
            this.checkUncheckContextMenuStrip.Size = new System.Drawing.Size(165, 70);
            // 
            // checkAllToolStripMenuItem
            // 
            this.checkAllToolStripMenuItem.Name = "checkAllToolStripMenuItem";
            this.checkAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.checkAllToolStripMenuItem.Text = "&Check All";
            this.checkAllToolStripMenuItem.Click += new System.EventHandler(this.checkAllCommentsToolStripMenuItem_Click);
            // 
            // uncheckAllToolStripMenuItem
            // 
            this.uncheckAllToolStripMenuItem.Name = "uncheckAllToolStripMenuItem";
            this.uncheckAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.uncheckAllToolStripMenuItem.Text = "&Uncheck All";
            this.uncheckAllToolStripMenuItem.Click += new System.EventHandler(this.uncheckAllCommentsToolStripMenuItem_Click);
            // 
            // deleteRowToolStripMenuItem
            // 
            this.deleteRowToolStripMenuItem.Name = "deleteRowToolStripMenuItem";
            this.deleteRowToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.deleteRowToolStripMenuItem.Text = "Delete Comment";
            this.deleteRowToolStripMenuItem.Click += new System.EventHandler(this.deleteRowToolStripMenuItem_Click);
            // 
            // GradeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.commentGridView);
            this.Controls.Add(this.pointValueRadioButtonGroupBox);
            this.Name = "GradeControl";
            this.Size = new System.Drawing.Size(518, 308);
            this.pointValueRadioButtonGroupBox.ResumeLayout(false);
            this.pointValueRadioButtonGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.commentGridView)).EndInit();
            this.checkUncheckContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox pointValueRadioButtonGroupBox;
        private System.Windows.Forms.RadioButton acceptableRadioButton;
        private System.Windows.Forms.RadioButton exceptionalRadioButton;
        private System.Windows.Forms.RadioButton unsatisfactoryRadioButton;
        private System.Windows.Forms.RadioButton amateurRadioButton;
        private System.Windows.Forms.DataGridView commentGridView;
        private System.Windows.Forms.ContextMenuStrip checkUncheckContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem checkAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uncheckAllToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectCommentToIncludeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentToIncludeColumn;
        private System.Windows.Forms.ToolStripMenuItem deleteRowToolStripMenuItem;
    }
}
