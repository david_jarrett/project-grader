﻿namespace Project1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.functionalityTabPage = new System.Windows.Forms.TabPage();
            this.functionalityGradeControl = new CustomControlLibrary.GradeControl();
            this.implementationTabPage = new System.Windows.Forms.TabPage();
            this.implementationGradeControl = new CustomControlLibrary.GradeControl();
            this.documentationTabPage = new System.Windows.Forms.TabPage();
            this.documentationGradeControl = new CustomControlLibrary.GradeControl();
            this.outputSummaryTextBox = new System.Windows.Forms.TextBox();
            this.copyButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.commentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.functionalityTabPage.SuspendLayout();
            this.implementationTabPage.SuspendLayout();
            this.documentationTabPage.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.functionalityTabPage);
            this.tabControl.Controls.Add(this.implementationTabPage);
            this.tabControl.Controls.Add(this.documentationTabPage);
            this.tabControl.Location = new System.Drawing.Point(12, 27);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(554, 333);
            this.tabControl.TabIndex = 1;
            // 
            // functionalityTabPage
            // 
            this.functionalityTabPage.Controls.Add(this.functionalityGradeControl);
            this.functionalityTabPage.Location = new System.Drawing.Point(4, 22);
            this.functionalityTabPage.Name = "functionalityTabPage";
            this.functionalityTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.functionalityTabPage.Size = new System.Drawing.Size(546, 307);
            this.functionalityTabPage.TabIndex = 0;
            this.functionalityTabPage.Text = "Functionality";
            this.functionalityTabPage.UseVisualStyleBackColor = true;
            // 
            // functionalityGradeControl
            // 
            this.functionalityGradeControl.AcceptableRadioButtonPointValue = 2;
            this.functionalityGradeControl.AmateurRadioButtonPointValue = 1;
            this.functionalityGradeControl.ExceptionalRadioButtonPointValue = 3;
            this.functionalityGradeControl.Location = new System.Drawing.Point(14, 0);
            this.functionalityGradeControl.Name = "functionalityGradeControl";
            this.functionalityGradeControl.Size = new System.Drawing.Size(518, 285);
            this.functionalityGradeControl.TabIndex = 0;
            this.functionalityGradeControl.Title = "Default";
            this.functionalityGradeControl.UnsatisfactoryRadioButtonPointValue = 0;
            // 
            // implementationTabPage
            // 
            this.implementationTabPage.Controls.Add(this.implementationGradeControl);
            this.implementationTabPage.Location = new System.Drawing.Point(4, 22);
            this.implementationTabPage.Name = "implementationTabPage";
            this.implementationTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.implementationTabPage.Size = new System.Drawing.Size(546, 307);
            this.implementationTabPage.TabIndex = 1;
            this.implementationTabPage.Text = "Implementation";
            this.implementationTabPage.UseVisualStyleBackColor = true;
            // 
            // implementationGradeControl
            // 
            this.implementationGradeControl.AcceptableRadioButtonPointValue = 2;
            this.implementationGradeControl.AmateurRadioButtonPointValue = 1;
            this.implementationGradeControl.ExceptionalRadioButtonPointValue = 3;
            this.implementationGradeControl.Location = new System.Drawing.Point(14, 0);
            this.implementationGradeControl.Name = "implementationGradeControl";
            this.implementationGradeControl.Size = new System.Drawing.Size(518, 285);
            this.implementationGradeControl.TabIndex = 0;
            this.implementationGradeControl.Title = "Default";
            this.implementationGradeControl.UnsatisfactoryRadioButtonPointValue = 0;
            // 
            // documentationTabPage
            // 
            this.documentationTabPage.Controls.Add(this.documentationGradeControl);
            this.documentationTabPage.Location = new System.Drawing.Point(4, 22);
            this.documentationTabPage.Name = "documentationTabPage";
            this.documentationTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.documentationTabPage.Size = new System.Drawing.Size(546, 307);
            this.documentationTabPage.TabIndex = 2;
            this.documentationTabPage.Text = "Documentation";
            this.documentationTabPage.UseVisualStyleBackColor = true;
            // 
            // documentationGradeControl
            // 
            this.documentationGradeControl.AcceptableRadioButtonPointValue = 2;
            this.documentationGradeControl.AmateurRadioButtonPointValue = 1;
            this.documentationGradeControl.ExceptionalRadioButtonPointValue = 3;
            this.documentationGradeControl.Location = new System.Drawing.Point(14, 0);
            this.documentationGradeControl.Name = "documentationGradeControl";
            this.documentationGradeControl.Size = new System.Drawing.Size(518, 285);
            this.documentationGradeControl.TabIndex = 0;
            this.documentationGradeControl.Title = "Default";
            this.documentationGradeControl.UnsatisfactoryRadioButtonPointValue = 0;
            // 
            // outputSummaryTextBox
            // 
            this.outputSummaryTextBox.Location = new System.Drawing.Point(14, 367);
            this.outputSummaryTextBox.Multiline = true;
            this.outputSummaryTextBox.Name = "outputSummaryTextBox";
            this.outputSummaryTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputSummaryTextBox.Size = new System.Drawing.Size(547, 207);
            this.outputSummaryTextBox.TabIndex = 4;
            this.outputSummaryTextBox.TabStop = false;
            // 
            // copyButton
            // 
            this.copyButton.Location = new System.Drawing.Point(204, 586);
            this.copyButton.Name = "copyButton";
            this.copyButton.Size = new System.Drawing.Size(75, 23);
            this.copyButton.TabIndex = 2;
            this.copyButton.Text = "&Copy";
            this.copyButton.UseVisualStyleBackColor = true;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(296, 586);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 3;
            this.clearButton.Text = "Clea&r";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commentsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(575, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // commentsToolStripMenuItem
            // 
            this.commentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.commentsToolStripMenuItem.Name = "commentsToolStripMenuItem";
            this.commentsToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.commentsToolStripMenuItem.Text = "C&omments";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.importToolStripMenuItem.Text = "&Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.exportToolStripMenuItem.Text = "&Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 621);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.copyButton);
            this.Controls.Add(this.outputSummaryTextBox);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assignment Grader by David Jarrett";
            this.tabControl.ResumeLayout(false);
            this.functionalityTabPage.ResumeLayout(false);
            this.implementationTabPage.ResumeLayout(false);
            this.documentationTabPage.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage functionalityTabPage;
        private System.Windows.Forms.TabPage implementationTabPage;
        private System.Windows.Forms.TabPage documentationTabPage;
        //private CustomControlLibrary.GradeControl dataFormControl1;
        private CustomControlLibrary.GradeControl functionalityGradeControl;
        private CustomControlLibrary.GradeControl implementationGradeControl;
        private CustomControlLibrary.GradeControl documentationGradeControl;
        private System.Windows.Forms.TextBox outputSummaryTextBox;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem commentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
    }
}

