﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CustomControlLibrary;
using CustomControlLibrary.io.report;
using CustomControlLibrary.io.serialize;
using Project1.io.report;

namespace Project1
{
    /// <summary>
    /// This is the primary form for the application.
    /// </summary>
    /// <remarks>
    /// Author:     David Jarrett
    /// Version:    01/26/2018
    /// </remarks>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class MainForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// <remarks>
        /// Precondition:   None
        /// Postcondition:  The application will be initialized and running.
        /// </remarks>
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();

            this.initializeFunctionalityGradeControl();
            this.initializeImplementationGradeControl();
            this.initializeDocumentationGradeControl();

            this.processOnDataStateChangedEvent(this, EventArgs.Empty);
        }

        private void initializeFunctionalityGradeControl()
        {
            this.functionalityGradeControl.Title = "Functionality";
            this.setRadioButtonPointValuesOf(this.functionalityGradeControl, 10, 7, 3, 0);
            this.addCommentsToGradeControl(this.functionalityGradeControl, new [] {
                "Well done.", "No issues were discovered."
            });

            this.functionalityGradeControl.DataStateChanged += this.processOnDataStateChangedEvent;
        }

        private void initializeImplementationGradeControl()
        {
            this.implementationGradeControl.Title = "Implementation";
            this.setRadioButtonPointValuesOf(this.implementationGradeControl, 7, 5, 3, 1);
            this.addCommentsToGradeControl(this.implementationGradeControl, new [] {
                "Magic numbers are used within the code.",
                "Overall class design not following best practices.",
                "Violations of DRY need addressed."
            });

            this.implementationGradeControl.DataStateChanged += this.processOnDataStateChangedEvent;
        }

        private void initializeDocumentationGradeControl()
        {
            this.documentationGradeControl.Title = "Documentation";
            this.setRadioButtonPointValuesOf(this.documentationGradeControl, 3, 2, 1, 0);
            this.addCommentsToGradeControl(this.documentationGradeControl, new [] {
                "Good job.", "Missing lots of required documentation."
            });
            
            this.documentationGradeControl.DataStateChanged += this.processOnDataStateChangedEvent;
        }

        private void setRadioButtonPointValuesOf(GradeControl gradeControl, int exceptional,
            int acceptable, int amateur, int unsatisfactory)
        {
            gradeControl.ExceptionalRadioButtonPointValue = exceptional;
            gradeControl.AcceptableRadioButtonPointValue = acceptable;
            gradeControl.AmateurRadioButtonPointValue = amateur;
            gradeControl.UnsatisfactoryRadioButtonPointValue = unsatisfactory;
        }

        private void addCommentsToGradeControl(GradeControl gradeControl, IEnumerable<string> comments)
        {
            foreach (var currentComment in comments)
            {
                gradeControl.AddComment(currentComment);
            }
        }

        private void processOnDataStateChangedEvent(object sender, EventArgs e)
        {
            var gradeControls = this.getAllGradeControls();
            IGradeReporter reportGenerator = new MultiControlGradeReporter(gradeControls);
            this.outputSummaryTextBox.Text = reportGenerator.GenerateReport();
        }

        private List<GradeControl> getAllGradeControls()
        {
            var gradeControls = new List<GradeControl>();
            foreach (TabPage currentPage in this.tabControl.TabPages)
            {
                var gradeControl = this.getGradeControlFor(currentPage);
                gradeControls.Add(gradeControl);
            }
            return gradeControls;
        }

        private GradeControl getGradeControlFor(Control currentPage)
        {
            return currentPage.Controls.Cast<GradeControl>().FirstOrDefault();
        }


        private void copyButton_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.outputSummaryTextBox.Text);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            this.resetForm();
        }

        private void resetForm()
        {
            foreach (TabPage currentPage in this.tabControl.TabPages)
            {
                var gradeControl = this.getGradeControlFor(currentPage);
                gradeControl?.ResetControl();
            }
            this.processOnDataStateChangedEvent(this, EventArgs.Empty);
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show(@"This will clear all currently loaded comments!",
                @"Are you sure?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.importCommentsIntoGradeControls();
                this.processOnDataStateChangedEvent(this, EventArgs.Empty);
            }
        }

        private void importCommentsIntoGradeControls()
        {
            var commentCollections = CommentSerializer.Deserialize();
            var commentCollectionIndex = 0;

            if (commentCollections == null)
            {
                MessageBox.Show(@"No comments have been exported yet. Nothing to load.", @"File Not Found");
                return;
            }

            foreach (TabPage currentPage in this.tabControl.TabPages)
            {
                var gradeControlForCurrentPage = this.getGradeControlFor(currentPage);
                var commentCollectionForCurrentPage = commentCollections[commentCollectionIndex++];

                gradeControlForCurrentPage.ClearAllComments();
                this.addCommentsToGradeControl(gradeControlForCurrentPage, commentCollectionForCurrentPage.Comments);
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show(@"Export all comments?", @"Are you sure?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.exportCommentsFromAllGradeControls();
            }
            
        }

        private void exportCommentsFromAllGradeControls()
        {
            var gradeControls = this.getAllGradeControls();
            CommentSerializer.Serialize(gradeControls);
            MessageBox.Show(@"Comments have been exported.");
        }

    }
}
